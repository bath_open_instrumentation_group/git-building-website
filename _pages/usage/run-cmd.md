---
layout: page
title: Running GitBuilding From the Command Line
permalink: /usage/run-cmd
---


All the commands below assume you have successfully installed `gitbuilding` with `pip`.

If you just want to start the web application from the commandline see the [basic running instructions]({{site.baseurl}}/usage/run)

## Starting a new empty project

Open your terminal in an empty folder you want to use for your documentation and run

    gitbuilding new

empty documentation files will be added to the directory.

## Previewing the documentation and editing in the live editor

GitBuilding has a built in live editor. Open your terminal, navigate to the folder with your documentation, and run

    gitbuilding serve

You can now open a browser and navigate to `http://localhost:6178/`. This will show the documentation in a browsable form, exactly as it is will be output by `build-html` (see below). You can also edit the documentation directly from your browser by selecting `edit` in the top-right corner:

![]({{site.baseurl}}/assets/LiveEditorScreenshot.png)

> See [getting started]({{site.baseurl}}/usage/getting-started/) on how to correctly format what you write in the text box here.

## Building to standard markdown documentation

To build the documentation in your current folder run

    gitbuilding build

This will create markdown documentation in the directory `_build`. This documentation can then be used anywhere you might usually use markdown.

## Build a static-html site

You can also use GitBuilding to create a static html with

    gitbuilding build-html

This will output your an html website into `_site`. This is output is designed to be served by a server, allowing 3D viewing of models, and automatically serving html pages without `.html` appened to each link. For a more basic site that will open locally in a browser run

    gitbuilding build-html --no-server

## Creating PDF documentation (experimental)

If you have installed WeasyPrint (see instructions above) GitBuilding can create PDFs. To create a pdf run

    gitbuilding build-pdf

This will output a pdf in the `_pdf` directory. For [complex documentation that defines multiple variations of a piece of hardware]({{site.baseurl}}/usage/complex-projects#documentation-reuse), one PDF per variation is produced.

## Continuous integration

For those hosting their documentation on a platform such as GitHub or GitLab "continuous integration" will create and host a website of your documentation. To set up continuous integration run:

    gitbuilding generate ci

and follow the instructions to chose the hosting service you wish to use. The current CI scripts assume that your documentation is in the root directory of your Git repository. We will improve this feature to make it more intelligent and flexible.

## Getting help

You can get more detail on the command line options by running

    gitbuilding help

You can get help for a specific command with

    gitbuilding help <command>

If you still have problems please [raise an issue on GitLab](https://gitlab.com/gitbuilding/gitbuilding/issues).
