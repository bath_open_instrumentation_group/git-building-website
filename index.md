---
layout: home
---

# GitBuilding is an open source program for writing hardware assembly instructions.

Keeping documentation up to date while you are prototyping is essential if working in a large or distributed team. But keeping complex documentation and Bills of Materials in sync can be difficult. GitBuilding automates many of these tasks so you can stop worrying about documentation and get back to building something awesome.

**Stop worrying, GitBuilding!**


## GitBuilding allows you to:

- Write assembly instructions as you are prototyping, in Markdown, using a two-paned editor that previews the final output.
- Tag links in your text with meta-data (part specs, descriptions, quantities); marking them as pre-requisite assembly steps or parts you need to purchase.
- You can also efficiently bulk-define part meta-data using separate part libraries.<!-- or spreadsheet formats.-->
- Automatically generate independent documentation and final bills of materials for multiple variations of your project.
- Generate previews of 3D files, and automatically zip only the production files that are needed for a specific variation.
- Export your documentation in Markdown, HTML, or PDF, and your final BOM as a CSV.
- Keep track of your evolving documentation within any version control system that supports plain text (such as Git).


<div style="width:80%; display:flex; justify-content:space-around; margin:55px; flex-wrap:wrap;">

<a href="{{site.baseurl}}/install" class="custom-button">
Download & Install
</a>

<a href="{{site.baseurl}}/usage/run" class="custom-button outline">
Running GitBuilding
</a>


</div>


## Keep control of your documentation

GitBuilding uses [Markdown](https://commonmark.org/help/) with some [additional syntax]({{site.baseurl}}/usage/buildup). We call this BuildUp. Despite introducing a new format you never risk lock-in. GitBuilding is both open source, and it can export your documentation to plain Markdown. If you wish to move your documentation away from GitBuilding you can export it as plain Markdown.

GitBuilding is beginning to mature, but the syntax still may change. We are open to feedback and suggestions. If you want to get involved please visit the [project on GitLab](https://gitlab.com/gitbuilding/gitbuilding) or get in contact via [Gitter](https://gitter.im/gitbuilding/community) or using [our space on the GOSH forum](https://forum.openhardware.science/c/projects/gitbuilding/55).

## Examples of GitBuilding Documentation

* [OpenFlexure Microscope](https://build.openflexure.org/openflexure-microscope/latest)  [(source)](https://gitlab.com/openflexure/openflexure-microscope/-/tree/master/docs?ref_type=heads)
* [OpenFlexure Field Dissection Microscope](https://openflexure.gitlab.io/openflexure-field-dissection-microscope)  [(source)](https://gitlab.com/openflexure/openflexure-field-dissection-microscope/-/tree/master/docs?ref_type=heads)
* [autohaem smear](https://autohaem.gitlab.io/autohaem-smear/v2.0.0/) [(source)](https://gitlab.com/autohaem/autohaem-smear/-/tree/master/docs?ref_type=heads)

## GitBuilding is used by

![Organisations using GitBuilding]({{site.baseurl}}/assets/UsedBy.png)
