---
layout: page
title: Running GitBuilding
permalink: /usage/run
---
<div class="info-block" markdown="1">
These features were added in GitBuilding v0.13.0. Upgrade any older version before following the tutorial below.
</div>

For new users we recommend running the GitBuilding GUI or web application. This will let you create projects, edit the documentation, and modify the project settings. Some more advance file management may still require modifying files outside GitBuilding.

We are working on launching GitBuilding without needing a terminal. For now you still need to open up a terminal (such as powershell in Windows). Once Gitbuilding is [installed]({{site.baseurl}}/install) you should be able to run the following command in your terminal:

    gitbuilding-gui

If you didn't install the GUI you can run the same application in a browser tab. Run:

    gitbuilding webapp

Then open your browser and navigate to `http://localhost:6178/`


## The UI

When you first open the application it should look like this:

![]({{site.baseurl}}/assets/GUI1.png)


If you already have a GitBuilding project on your computer you can click "Open project" and navigate to the project:

![]({{site.baseurl}}/assets/GUI-Open.png)

*(Note, for now you can only navigate within the directory where you started the application)*

If you want to create a new project click the "New Project button" and fill in the the project information:

![]({{site.baseurl}}/assets/GUI-New1.png)

Once a project has been opened or created it will make a card in the projects view. If you click "Start Editor" for a project GitBuilding will create a live editable view of the projects documentation.

![]({{site.baseurl}}/assets/GUI-New2.png)

Once ready the button will turn green and read "Start Editor". Click it again and a browser tab will open with your project.

## Editing your project in the live editor

![]({{site.baseurl}}/assets/NewProject.png)

You can navigate your project's documentation. You can create new pages, or edit existing paged with the buttons in the top right. When you edit a page, a side-by-side editor will open.

![]({{site.baseurl}}/assets/LiveEditorScreenshot.png)

See our [getting started]({{site.baseurl}}/usage/getting-started/) guide for how to edit the documentation.

From any page in the documentation (if you are not in the editor), you should have a settings button in the top right. In the settings menu you can select "Project Settings" to customise your project.

![]({{site.baseurl}}/assets/ConfigEditorScreenshot.png)

## Advanced usage

For more advanced usage see the [Command Line Interface]({{site.baseurl}}/usage/run-cmd).
