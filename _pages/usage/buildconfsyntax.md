---
layout: page
title: Configuring the GitBuilding output
permalink: /usage/buildconfsyntax
---

<div class="info-block" markdown="1">
**Attention Beginner Users!**

Almost everything on this page can be achieved from the "Project Settings" interface in the [live editor]({{site.baseurl}}/usage/run#editing-your-project-in-the-live-editor).
</div>

This page explains how you can manually configure the output of GitBuilding by editing configuration files. Most of the customisation is done by putting information in  a [YAML] file called `buildconf.yaml`. You can also customise the HTML output from GitBuilding by editing the `buildconf.yaml` file and adding files to the `assets` directory.

[YAML]: https://en.wikipedia.org/wiki/YAML

<h3>Table of contents</h3>
* Do not remove this line (it will not be displayed)
{:toc}

## Recommended information

It is recommended to add following keys to the `buildconf.yaml` to add important information:

* **Authors**: Either a list of authors or the name of the author
* **Affiliation**: A group the project is affiliated with
* **License**: The licence that the project is released under. If this is SPDX license identifier that GitBuilding supports, the licence file will automatically be included. If not you should include a copy of the license file in your documentation folder.
* **LicenseFile**: If the license specified is not a recognised SPDX license identifier then this should be a relative link to the license file in your documentation folder.
* **Email**: Contact email address

## Categories and parts

The `buildconf.yaml` file can be used to customise the categories for parts in your project.

As standard there are two categories: `Part` and `Tool`. The main difference is how the are counted. Parts are added, whereas for tools the maximum number used in any step is recorded. The logic for this is that if you need 1 M3 nut and a 5.5 mm nut driver in one step, and 1 M3 nut and a 5.5 mm nut driver in a later step, then you will need two M3 nuts but only one 5.5 mm nut driver. To set these addition rules `Part` has a `Reuse` property set to `False`, `Tool` has a reuse property set to `True`. 

Categories also have a `DisplayName`. When you set the category in a part link you use its name (`Part` or `Tool`), but when it is displayed in a bill of materials the display name is used ("Parts" and "Tools").

An example custom category might be:

<pre class="example-block">
{% raw %}
CustomCategories:
    PrintedTool:
        Reuse: True
        DisplayName: Printed tools
{% endraw %}
</pre>

As default anything with a category not explicitly stated is assumed to be a `Part`. The default category can be changed with:

<pre class="example-block">
{% raw %}
DefaultCategory: PrintedTool
{% endraw %}
</pre>

## Custom navigation

 
You can override the auto-generated navigation by defining the navigation key as below:

<pre class="example-block">
{% raw %}
Navigation:
  - Link: relative_url_1
    Title: LinkName1
  - Link: relative_url_2
    Title: LinkName2
    SubNavigation:
      - Link: sublink_url
        Title: SubLinkName
{% endraw %}
</pre>

The `SubNavigation` key is optional, if not provided it wil auto-populate with any steps defined on the linked page. To turn off this auto-population of sub-navigations add the following line to your config file:

<pre class="example-block">
{% raw %}
AutoCompleteSubNav: False
{% endraw %}
</pre>

## Using files from external directories
*Added in GitBuilding v0.12*

GitBuilding as standard expects that all files that are linked to are contained within the documentation directory (the directory where the configuration file is stored, and from where GitBuilding is run). This is to help ensure the documentation is portable, and because it is unclear where the file should be stored within the output documentation.

In certain circumstances it is useful for files outside the documentation directory to be accessible. For example if a project has two directories: `docs`, `CAD`, where the `CAD` directory contained the STLs that the documentation will want to link to. If a link is made from a documentation page to `[../CAD/widget.stl]` GitBuilding will warn about the `widget.stl` being orphaned, as it is outside the expected directory. However, if we add the following to the configuration file:

<pre class="example-block">
{% raw %}
ExternalDirs:
    cad-files: "../CAD"
{% endraw %}
</pre>

Then when linking to `[../CAD/widget.stl]` no error will be produced. In the final documentation the file will be located at `/cad-files/widget.stl`.

**Note:** Markdown files in external directories are not processed as BuildUp.

## Customising HTML output

When you run `gitbuilding serve` or `gitbuilding build-html` GitBuilding will generate an HTML website for your documentation. This output can be customised as follows:

### YAML settings for style

*Added in GitBuilding v0.13*

<div class="info-block" markdown="1">
**Note:** These style settings could be overwritten with if you have a custom stylesheet as explained below.
</div>

The `buildconf.yaml` file can be used to specify a few style elements for generating a simple theme:

* **PrimaryColor**: This should be a string specifying a hexadecimal colour code. For example "#bb0000"
* **SecondaryColor**: This should be a string specifying a hexadecimal colour code. For example "#bb0000"
* **HoverColor**: This should be a string specifying a hexadecimal colour code. For example "#bb0000"
* **RemoveImagesBorder**: Boolean value. Affects how image galleries are displayed.
* **RemoveImagesBackground**: Boolean value. Affects how image galleries are displayed.


### Custom Header and Footer

The header and footer are generated from [jinja2 templates](https://jinja.palletsprojects.com/en/2.11.x/). If you make a directory in your documentation called `_templates` and put in files called `header.html.jinja` or `footer.html.jinja` it will use these instead of the default ones. This feature is not well documented but [you can look at the standard templates for inspiration](https://gitlab.com/gitbuilding/gitbuilding/-/tree/master/gitbuilding/templates) (in theory any of these could be modified by adding a modified copy to `_templates`), there is also an example custom header and footer in [the example project](https://gitlab.com/gitbuilding/gitbuilding-example/).

When making custom templates the following variables can be accessed:

* `title` - Project title
* `year` - The current year
* `root` - The website root (see below)
* `authors` - Authors of the project
* `email` - Contact email for the project
* `affiliation` - Project affiliation
* `license` - Project licence, will link to the licence if recognised.
* `landing` - The url of the project landing page

Custom variables can be used by specifying them in the `buildconf.yaml` file as follows:

<pre class="example-block">

Variables:
    version: v6.2
HTMLOptions:
    CustomFooter: >
        &lt;p class="version">The current version of this project is {version}&lt;/p>

</pre>

The footer would be modified to
<pre class="example-block">

&lt;p class="version">The current version of this project is v6.2&lt;/p>

</pre>


### Custom CSS and favicon

GitBuilding scans for a directory called `assets` in the root directory. If this directory exists and css files inside it are included in the `<head>` of the HTML. These style sheets can then be used to override the default CSS of GitBuilding.

The `assets` directory is also scanned for a `favicon.ico` file or any PNG favicon which specifies its size (i.e. `favicon-16x16.png`). If any files are matched they are used as the favicon for the documentation instead of the GitBuilding favicon.

*The PNG favicon will only be found if it matches the following regular expression:*  
`^favicon-[0-9]+x[0-9]+\.png$`


### Turning off search

*Added in GitBuilding v0.14*

By default a javascript search bar will be added above the navigation to allow readers to search within the documentation. To turn this off in the `buildconf.yaml` file you can use the option:

* **IncludeSearch**: Set to False to remove the search functionality from your documentation  *Default: True*

## Customising PDF output

*Added in GitBuilding v0.14*

GitBuilding doesn't output every page to the PDF documentation, this often generated huge PDFs with lots of extra pages for complex projects.

As standard only the pages on the main path through the documentation are included. By this we mean the pages you see if you follow the "next" and "previous" links at the bottom of the pages.

This can be customised in the buildconf.yaml` file using the following option:

* **ExtraPagesForPDF**: Specifies a list of pages that should also be included in the PDF documentation.

## Other config options

Other options for the `buildconf.yaml` file:

* **LandingPage**: Specifies which markdown file should become index.html in an HTML build. Not needed if one file is called `index.md`
* **WebsiteRoot**: Sets the root domain for links in the static HTML website that can be built with `gitbuilding build-html` *Default: '/'* **[Note that as GitBuilding now uses relative links for all pages this will be depreciated]**
* **PageBOMTitle**: Sets a message to be displayed above the bill of materials on a page when the syntax {% raw %}`{{BOM}}`{% endraw %} is used.
* **Exclude**: List of files that are in the documentation directory that you do not want to be processed. (Note: all files beginning with a `.` or a `_` are also excluded.  *Default: [README.md]*
* **ForceOutput**: This is a list of files (or [glob](https://en.wikipedia.org/wiki/Glob_%28programming%29) patterns) that are forced to be in the output. As standard non-markdown files are only included in the output if they are linked to in a BuildUp file. *Default is an empty list*
* **Title**: Can be used to override the title of the project for the HTML build. Default is the title of index.md or the `LandingPage`
* **Fussy**: Boolean value. If true GitBuilding will warn about possible problems with the documentation. *Default: True*
* **RemoveBottomNav**: Boolean value. If true GitBuilding will not generate the "Previous" and "Next" buttons at the bottom of relevant pages. *Default: False*

