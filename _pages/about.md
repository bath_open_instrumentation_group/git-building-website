---
layout: page
title: About GitBuilding
permalink: /about/
---

GitBuilding was initially set up to help with documenting the [OpenFlexure project](https://openflexure.org). It has been shaped by ongoing discussions with other projects in the open hardware community, especially the [GOSH community](https://forum.openhardware.science).

GitBuilding is grateful to have received funding from:

* [Open Source Ecology Germany](https://www.ose-germany.de/)
* [LibreHub](https://librehub.github.io/)
* [OpenMake](https://www.openmake.de)
* [nlnet foundation](https://nlnet.nl/project/HardwareManuals/)

GitBuilding source code and issue trackers are on GitLab. Feel free to [raise an issue](https://gitlab.com/gitbuilding/gitbuilding/issues) there if you are experiencing problems or have any suggestions. You can also come get in contact with us [our space on the GOSH forum](https://forum.openhardware.science/c/projects/gitbuilding/55).

