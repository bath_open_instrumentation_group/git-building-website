---
layout: page
title: Installing GitBuilding
permalink: /install
---


## Installation

We are still working on developing a stand alone installer for GitBuilding. For now you need Python installed on your computer. GitBuilding is then installed with Pip (the Package Installer for Python).

To install (for Python 3.7+)

    pip install gitbuilding[gui]

If you have problems try updating pip with:

    pip install --upgrade pip

and then try again.

If you do not want to run the standalone GUI (you can still run the webapp from the browser) you install gitbuilding without installing PyQT by running:

    pip install gitbuilding

If you wish to contribute to development of GitBuilding you can clone the project from GitLab but you will need to build the javascript editor from source. For more information on contributing see our [contributing guide](https://gitlab.com/gitbuilding/gitbuilding/-/blob/master/CONTRIBUTING.md).

Once the GitBuilding is a bit more stable we will add a simple way to install and run without using the command line.

**Next: see [how to run GitBuilding]({{site.baseurl}}/usage/run/).**

### For PDF generation

To generate PDFs GitBuilding uses WeasyPrint. Please follow the [instructions on the WeasyPrint website](https://doc.courtbouillon.org/weasyprint/stable/first_steps.html#installation). We are working on bundling this into an installer.

